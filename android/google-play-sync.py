# SPDX-FileCopyrightText: 2021 Volker Krause <vkrause@kde.org>
# SPDX-License-Identifier: LGPL-2.0-or-later

import argparse
import os
import re
import subprocess
import zipfile

# NOTE: this assumes to be run inside the environment created
# by sourcing setup-fastlane.sh!

# Copy inFileName to outFileName and apply variable replacements
def copyTemplate(inFileName, outFileName, replacements = {}):
    f = open(os.path.join(os.path.dirname(__file__), inFileName), 'r')
    content = f.read()
    f.close()
    for key in replacements:
        content = re.sub(f"\\{{{key}\\}}", replacements[key], content)
    f = open(outFileName, 'w')
    f.write(content)
    f.close()


# command line interface
parser = argparse.ArgumentParser(description='Utility to upload metadata to Google Play')
parser.add_argument('--app-id', type=str, required=True)
parser.add_argument('--metadata', type=str, required=True)
parser.add_argument('--apk', type=str, required=True)
arguments = parser.parse_args()

#
# Configure Fastlane for this app
#
workdir = os.path.join(os.environ['FASTLANE_WORKDIR'], arguments.app_id)
os.makedirs(os.path.join(workdir, 'fastlane'), exist_ok = True)

# write Gemfile
copyTemplate('Gemfile.in', os.path.join(workdir, 'Gemfile'))
# remove Gemfile.lock, otherwise the shared updates wont be visible in this remains
# pinned to the first fastlane version it ever saw
lockfile = os.path.join(workdir, 'Gemfile.lock')
if os.path.exists(lockfile):
    os.unlink(lockfile)

# write Appfile
copyTemplate('Appfile.in', os.path.join(workdir, 'fastlane/Appfile'), {'HOME': os.environ['HOME'], 'app_id': arguments.app_id})

#
# Download existing meta data for comparisson
#
subprocess.run([os.environ['BUNDLE_EXECUTABLE'], 'exec', 'fastlane', 'supply', 'init' ], cwd = workdir)


#
# check whether there are metadata updates
#
textChanged = False
imageChanged = False
with zipfile.ZipFile(arguments.metadata, 'r') as z:
    for fileName in z.namelist():
        # directories or top-level elements
        if not '/' in fileName or fileName[-1] == '/':
            continue

        # determine the file name in the fastlane structure
        idx = fileName.index('/') + 1
        outFileName = os.path.join(workdir, 'fastlane', 'metadata', 'android', fileName[idx:])
        # adjust file names for screenshots
        outFileName = re.sub(r'/android/([^/]*)/images/([^/]*)/(\d)-.*\.png', r'/android/\1/images/\2/\3_\1.png', outFileName)

        inFile = z.open(fileName)
        inData = inFile.read()
        inFile.close()

        outData = ''
        if os.path.exists(outFileName):
            outFile = open(outFileName, 'rb')
            outData = outFile.read()
            outFile.close()

        if inData == outData:
            print(f"comparing '{fileName}' and '{outFileName}'... no change")
            continue

        print(f"comparing '{fileName}' and '{outFileName}'... needs updating")
        os.makedirs(os.path.dirname(outFileName), exist_ok = True)
        outFile = open(outFileName, 'wb')
        outFile.write(inData)
        outFile.close()

        # remember what we need to update
        if outFileName[-4:] == '.png':
            imageChanged = True
        else:
            textChanged = True


#
# Upload metadata changes if necessary
#
if not textChanged and not imageChanged:
    print('No metadata changes found.')
else:
    print(f"Needs image asset upload: {imageChanged}")
    # write Fastfile
    copyTemplate('Fastfile.in', os.path.join(workdir, 'fastlane/Fastfile'), {'app_id': arguments.app_id, 'skipImages': 'false' if imageChanged else 'true'})
    # submit updates
    subprocess.run([os.environ['BUNDLE_EXECUTABLE'], 'exec', 'fastlane', 'upload_metadata'], cwd = workdir)


#
# Upload APK
#
apkPath = os.path.abspath(arguments.apk)
subprocess.run([os.environ['BUNDLE_EXECUTABLE'], 'exec', 'fastlane', 'supply',
                '--apk', apkPath, '--track', 'beta', '--release_status', 'draft',
                '--skip_upload_images', 'true', '--skip_upload_screenshots', 'true'], cwd = workdir)
