// Request a node to be allocated to us
node( "StaticWeb" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First we need to gather all of the tools we will need to perform the build...
		stage('Retrieving Utilities') {
			// Grab the Binary Factory Tooling (for our tools and configuration)
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'bf-tooling/']],
				userRemoteConfigs: [[url: 'https://invent.kde.org/sysadmin/binary-factory-tooling.git']]
			]

			// We also need the Repository Metadata too (to be able to clone all of the repositories)
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'repo-metadata/']],
				userRemoteConfigs: [[url: 'https://invent.kde.org/sysadmin/repo-metadata.git']]
			]

			// As well as KApiDox to actually build the API Documentation
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'dockerize']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'kapidox/']],
				userRemoteConfigs: [[url: 'https://invent.kde.org/frameworks/kapidox.git']]
			]
		}

		// With the utilities retrieved, we can start by cloning the sources we are going to generate the API Documentation of
		stage('Cloning Sources') {
			// Clone the sources
			// Once that is done, retrieve the Dependency Diagrams from the CI system
			sh """
				mkdir sources/
				cd sources/
				for repositoryRule in `cat ../bf-tooling/apidocs/repos-to-process`; do
					python3 ../repo-metadata/git-helpers/git-kclone "\$repositoryRule"
				done
				cd ../
			"""
		}

		// With the sources available to us, we can now build them...
		stage('Building') {
			// Build the API Docs!!
			sh """
				export PATH=/usr/lib64/qt5/bin/:\$WORKSPACE/kapidox/:\$PATH

				mkdir generated-docs/
				cd generated-docs/
				python3 \$WORKSPACE/kapidox/kapidox/kapidox_generate.py --doxdatadir \$WORKSPACE/kapidox/kapidox/data/ --qtdoc-dir \$WORKSPACE/bf-tooling/apidocs/tags/5.15/ --qtdoc-link https://doc.qt.io/qt-5 --qtdoc-flatten-links \$WORKSPACE/sources/
			"""
		}

		// With the docs built, now we publish them
		stage('Publishing') {
			// Upload to our server...
			sh """
				rsync -Ha -e "ssh -i ~/WebsitePublishing/website-upload.key" generated-docs/ sitedeployer@nicoda.kde.org:/srv/www/generated/api-dev.kde.org/
			"""
		}

	}
}
}
